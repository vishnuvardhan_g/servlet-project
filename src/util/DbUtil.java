package util;

import java.sql.Connection;
import java.util.ResourceBundle;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DbUtil {
private DbUtil() {
  // TODO Auto-generated constructor stub
}
public static Connection getCon() throws ClassNotFoundException,SQLException {
  ResourceBundle rb= ResourceBundle.getBundle("db");
  Class.forName(rb.getString("driver"));
  Connection con=DriverManager.getConnection(rb.getString("url"),rb.getString("user"),rb.getString("password"));
  return con;
}
}
