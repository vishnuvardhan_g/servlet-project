package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.IPlayerDAO;
import dao.IPlayerDAOImpl;
import model.Players;


@WebServlet(name="AdminCrudController", urlPatterns= {"/admincrud","/home","/delete","/insert","/signout","/update"})
public class AdminCrudController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	  
	  String url=request.getServletPath();
	  IPlayerDAO dao=new IPlayerDAOImpl();
	  Players players=new Players();
	  
	  if (url.equals("/admincrud")) {
	    List<Players> list=dao.viewAll();
	      request.setAttribute("pla", list);
	      request.getRequestDispatcher("admincrud.jsp").include(request, response); 
	  }else if(url.equals("/home")) {
	    List<Players> list=dao.viewAll();
        request.setAttribute("pla", list);
        request.getRequestDispatcher("admincrud.jsp").forward(request, response);  
	  }else if(url.equals("/delete")) {
	    Integer playerid=Integer.parseInt(request.getParameter("playerid"));
	    players.setPlayerid(playerid);
	    dao.delete(players);
	    PrintWriter out = response.getWriter();
	    response.setContentType("text/html");
	    out.print(playerid+ " deleted succefully");
	    request.getRequestDispatcher("admincrud").include(request,response);
	  }else if(url.equals("/insert")) {
	    request.getRequestDispatcher("insert.jsp").include(request,response);
	  }else if(url.equals("/signout")) {
	    HttpSession session=request.getSession(false);
        Integer uid=(Integer)session.getAttribute("uid");
        System.out.println(uid+ "logged out @"+ new Date());
        session.invalidate();
        request.setAttribute("uid",  uid+ "logged out successfully");
        request.getRequestDispatcher("index.jsp").include(request,response);      
	  }else if(url.equals("/update")) {
	    Integer playerid=Integer.parseInt(request.getParameter("playerid"));
	    String pname=request.getParameter("pname");
	    String country=request.getParameter("country");
	    Players players1=new Players(playerid,pname,country);
	    request.setAttribute("pla", players1 );
	    request.getRequestDispatcher("update.jsp").include(request,response);  
	    
	    
	  }
	}

}
