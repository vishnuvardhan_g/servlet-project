package controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.IPlayerDAO;
import dao.IPlayerDAOImpl;
import model.Admin;
import model.Players;


@WebServlet(name = "AdminController" , urlPatterns= {"/login","/add","/updateCont"})
public class AdminController extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	  String url=request.getServletPath();
	  IPlayerDAO dao=new IPlayerDAOImpl();
	  if(url.equals("/login")) {
		Integer uid=Integer.parseInt(request.getParameter("uid"));
		String password=request.getParameter("password");
		HttpSession session=request.getSession(true);
		session.setAttribute("uid" , uid);
		System.out.println(uid+"logged in @"+new Date(session.getCreationTime()));
		Admin admin =new Admin(uid,password);
		int result=dao.adminLogin(admin);
		if (result > 0) {
		  List<Players> list=dao.viewAll();
		  request.setAttribute("pla", list);
		  request.getRequestDispatcher("admin.jsp").forward(request, response);
		}else {
		  request.setAttribute("error","hi "+uid+ ", Please check your credentials");
		  request.getRequestDispatcher("index.jsp").forward(request, response);
		}
		}else if (url.equals("/add")) {
		  int playerid=Integer.parseInt(request.getParameter("playerid"));
		  String pname=request.getParameter("pname");
		  String country=request.getParameter("country");
          Players players=new Players(playerid,pname,country);
		  int result=dao.add(players);
		  if (result> 0) {
		    request.setAttribute("msg", playerid+ " inserted successfully");
		  }else {
		    request.setAttribute("msg", playerid+ " not inserted successfully");
		  }
		  List<Players> list=dao.viewAll();
          request.setAttribute("pla", list);
          request.getRequestDispatcher("admincrud.jsp").forward(request, response);
		  
		}else if (url.equals("/updateCont")) {
          int playerid=Integer.parseInt(request.getParameter("playerid"));
          String pname=request.getParameter("pname");
          String country=request.getParameter("country");
          Players players=new Players(playerid,pname, country);
          int result=dao.add(players);
          if (result> 0) {
            request.setAttribute("msg", playerid+ " updated successfully");
          }else {
            request.setAttribute("msg", playerid+ " not updated successfully");
          }
          List<Players> list=dao.viewAll();
          request.setAttribute("pla", list);
          request.getRequestDispatcher("admincrud.jsp").forward(request, response);
	}

	}
}
