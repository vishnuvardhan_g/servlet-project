package dao;

import java.util.List;

import model.Admin;
import model.Players;

public interface IPlayerDAO {
public int adminLogin(Admin admin);
 public List<Players> viewAll(); 
 public void delete(Players players);
 public void update(Players players);
 public int add(Players players);
 

}
