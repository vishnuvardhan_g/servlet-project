package dao;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Admin;
import model.Players;
import util.DbUtil;
import util.QueryUtil;

public class IPlayerDAOImpl implements IPlayerDAO {
  int result;
  PreparedStatement pst;
  ResultSet rs;
  @Override
  public int adminLogin(Admin admin) {
 try {
   pst=DbUtil.getCon().prepareStatement(QueryUtil.adminLogin);
   pst.setInt(1,admin.getUid());
   pst.setString(2,admin.getPassword());
   rs=pst.executeQuery();
   while(rs.next()) {
     result++;
   }
 }catch(SQLException | ClassNotFoundException e) {
   
 }
    return result;
  }
  @Override
  public List<Players> viewAll() {
    List <Players> list=new ArrayList<Players>();
    try {
      pst=DbUtil.getCon().prepareStatement(QueryUtil.viewAll);
      rs=pst.executeQuery();
      while(rs.next()) {
       Players players=new Players(rs.getInt(1),rs.getString(2),rs.getString(3));
       list.add(players);
      }
    }catch(SQLException | ClassNotFoundException e) {
      
    }
    return list;
    
  }
  @Override
  public void delete(Players players) {
    try {
      pst=DbUtil.getCon().prepareStatement(QueryUtil.delete);
      pst.setInt(1, players.getPlayerid());
      pst.executeUpdate();
    }catch(SQLException | ClassNotFoundException e) {     
    }
    
  }
  @Override
  public void update(Players players) {
    try {
      pst=DbUtil.getCon().prepareStatement(QueryUtil.update);
      pst.setString(1, players.getPname());
      pst.setString(2, players.getCountry());
      pst.setInt(3, players.getPlayerid());
      pst.executeUpdate();
    }catch(SQLException | ClassNotFoundException e) {     
    }
    
  }
  @Override
  public int add(Players players) {
    result=0;
    try {
      pst=DbUtil.getCon().prepareStatement(QueryUtil.add);
      pst.setInt(1, players.getPlayerid());
      pst.setString(2, players.getPname());
      pst.setString(3, players.getCountry());
      result=pst.executeUpdate();
    }catch(SQLException | ClassNotFoundException e) {  
      return result;
    }
    return result;
    
  }

}
