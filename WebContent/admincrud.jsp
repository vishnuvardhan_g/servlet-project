<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core"prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin CRUD</title>
</head>
<body>
<h1> Franchise CRUD operations page</h1>
<p align="right" >Hi,<%=(Integer) session.getAttribute("uid") %>
<a href="home">Home</a>
<a href="insert">Add new player</a> 
<a href="signout">logout</a>
<table border="6">
<tr>
<th colspan="5"> Player Details</th></tr>
<tr>
<th>Player ID </th>
<th>Player Name </th>
<th> Player country</th>
<th>Update</th>
<th>Delete</th>
</tr>
<c:forEach items="${pla}" var="p" >
<tr style="text-align:center:">
<td>${p.getPlyerid()}</td>
<td> ${p.getPname()}</td>
<td> ${p.getCountry()}</td>
<td><a "style=text-declaration:none" href="update?playerid=${p.getPlayerid()}&pname=${p.getPname()}&country=${p.getCountry()}">Upadte</a></td>
<td><a "style=text-declaration:none" href="delete?playerid=${p.getPlayerid()}&pname=${p.getPname()}&country=${p.getCountry()}">Delete</a></td>
</tr>
</c:forEach>
</table>
</body>
</html>